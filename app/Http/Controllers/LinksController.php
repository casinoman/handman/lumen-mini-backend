<?php

namespace App\Http\Controllers;

use App\Models\Links;
use Illuminate\Http\Request;

class LinksController extends Controller
{
  public function showAll()
  {

    $formatted_articles = Links::all()->map(function ($item, $key) {
      $preview_array = explode(" ", $item["content"]);
      $preview_array_sliced = array_slice($preview_array, 0, 30);
      $preview_length = sizeof($preview_array);
      $item["content"] = join(" ", $preview_array_sliced) . ($preview_length > 30 ? "..." : "");
      return $item;
    });

    return response()->json($formatted_articles);
  }

  public function showOne($id)
  {
    return response()->json(Commandos::find($id));
  }

  public function create(Request $request)
  {
    $this->validate($request, [
      'tag' => 'required',
      'management_url' => 'required',
      'secret' => 'required',
      'last_check' => 'required',
    ]);
    $link_request = $request->toArray();
    array_push(array(
        "active" => false,
        "created_at" => now(),
        "updated_at" => now(),
        "last_check" => now(),
    ), $link_request);


    $link = Links::create($link_request);

    return response()->json($link_request, 201);
  }

  public function update($id, Request $request)
  {
    $article = Commandos::findOrFail($id);
    $this->validate($request, [
      'title' => 'required',
      'author' => 'required',
      'content' => 'required',
    ]);
    $article->update($request->all());
    return response()->json($article, 200);
  }


  
  public function delete($id)
  {
    Commandos::findOrFail($id)->delete();
    return response('Deleted successfully', 200);
  }
}