<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class Links extends Model
{
  use HasUuids;

  protected $fillable = [
    'tag', 'management_url', 'secret', 'last_check',
  ];

  protected $casts = [
    'active' => 'boolean',
];

  protected $hidden = [];
}
