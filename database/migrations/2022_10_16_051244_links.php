<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('links', function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->string('tag');
                $table->string('management_url');
                $table->string('secret');
                $table->boolean('active');
                $table->timestamp('last_check')->default()->useCurrent();
                $table->timestamp('created_at')->default()->useCurrent();
                $table->timestamp('updated_at')->default()->useCurrent();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
};
